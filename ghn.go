package main

import (
	"fmt"
	"time"
)

func worker(id int, tasks <-chan int, done chan<- bool) {
	// goroutine read the task from channel.
	for i := range tasks {
		fmt.Println("Worker-", id, "print the task number:", i)
		// sleep the thread 1s
		time.Sleep(time.Second)

		done <- true
	}
}

func main() {
	fmt.Println("Start process...")
	st := time.Now().Unix()

	// init number of tasks
	var numOfTasks, numOfWorker = 100, 5

	// init channel
	tasks := make(chan int, numOfTasks)
	done := make(chan bool, numOfTasks)

	// init gorountine to execute tasks.
	for i := 1; i <= numOfWorker; i++ {
		go worker(i, tasks, done)
	}

	// add task to tasks channel
	for i := 1; i <= numOfTasks; i++ {
		tasks <- i
	}

	close(tasks)

	// channel sync
	for i := 0; i < numOfTasks; i++ {
		<-done
	}

	message := fmt.Sprintf("Finish process... in %d(s)", time.Now().Unix()-st)
	fmt.Println(message)
}
